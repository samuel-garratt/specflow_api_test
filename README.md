# SpecFlowAPI.

This project uses C# to automation tests to verify that an API is working as expected.
[SpecFlow](https://specflow.org/) is used to describe the tests using Gherkin syntax.

# Initial Setup

Visual Studio will need to be installed. You can install it from [here](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=16)

# Running tests via command line

`dotnet test`

# Running via Visual Studio

* Select the runsettings file desired by selecting `Test->Select Settings file` from the menu bar (This may be within a global settings sub menu.)
* View all tests `Test->Test Explorer`
* Run tests by right clicking on an individual scenario or feature and selecting `Run`

# Visual Studio extension

Download [SpecFlow Visual Studio extension](https://marketplace.visualstudio.com/items?itemName=TechTalkSpecFlowTeam.SpecFlowForVisualStudio) 
and double click on it to install it. 

# ApiObjects

Under Lib/ApiObjects there are 2 sub folders

* ApiService - handles interacting with the API. A class will be created for each API
* Models - has classes modelling the structure of the data which is represented by JSON in the request and the response of the API