Feature: Puppy

  Scenario: Order a Puppy with a specific name
    Given I want a puppy with a name of 'Laddie'
	When I order a Puppy
	Then the puppy has the name specified

  Scenario: Puppy defaults to available
	  Given I order a Puppy
	  When I pick them up
	  Then their status should be available
	  And the puppy has the name specified  