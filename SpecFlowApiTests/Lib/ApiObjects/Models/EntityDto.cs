﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace SpecFlowApiTests.Lib.ApiObjects
{
    abstract class EntityDto
    {
        /// <summary>
        /// Sets default params
        /// </summary>
        public abstract void DefaultParameters();

        /// <summary>
        /// Called on create to create request body from serialising properties
        /// </summary>
        /// <returns></returns>
        //public abstract string CreateObj();

        public EntityDto()
        {
            DefaultParameters(); // Initialise default parameters
        }

        public string CreateObj()
        {
            // These settings would be common for all Create methods
            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore
            };
            return JsonConvert.SerializeObject(this, serializerSettings);
        }
    }
}
