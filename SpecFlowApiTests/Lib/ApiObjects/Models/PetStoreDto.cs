using SpecFlowApiTest.Lib.ApiObjects;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using SpecFlowApiTest.Lib.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SpecFlowApiTests.Lib.ApiObjects
{
    /// <summary>
    /// Represents data for creating a Pet
    /// </summary>
    class PetStoreDto : EntityDto
    {
        /// <summary>
        /// Unique Id of the pet. Should not be set on creating object
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Name to reference the pet by
        /// </summary>
        public string Name { get; set; }

        public string Status { get; set; }        

        /// <summary>
        /// Sets default params
        /// </summary>
        public override void DefaultParameters()
        {
            Name = "doggie";
            Status = "available";
        }               
    }
}
