using SpecFlowApiTest.Lib.Extensions;
using Newtonsoft.Json.Linq;
using SpecFlowApiTests.Lib.ApiObjects;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace SpecFlowApiTest.Lib.ApiObjects
{    
    public abstract class BaseApiService
    {
        /// <summary>
        /// Taken from Swagger. Part after hostname
        /// </summary>
        public abstract string SubPath { get; }        

        /// <summary>
        /// Description of API for display purposes
        /// </summary>
        public abstract string Description { get; }               

        public static HttpClient client = GetInformation.Client;

        public Uri ApiPath()
        {
            return new Uri(ApiService.ApiHost + SubPath);
        }

        /// <summary>
        /// Return full url of API by using the common path and the sub path
        /// </summary>
        /// <param name="subPath">Path after common path for the API</param>
        /// <returns></returns>
        public Uri ApiPath(string subPath)
        {
            return new Uri($"{ApiService.ApiHost}{SubPath}/{subPath}");
        }

        public string Get(int id)
        {
            return Get(id.ToString());
        }

        /// <summary>
        /// Get the object that is associated to the passed Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string Get(string id)
        {
            var uri = ApiPath(id);
            HttpResponseMessage response = client.GetAsync(uri).Result;
            string responseString = response.Content.ReadAsStringAsync().Result;
            responseString.VerifyUnexpectedCharacters();
            TrafficLogger.Message($"REQUEST {response.RequestMessage.Method} {response.RequestMessage.RequestUri} \n " +
                $"Headers: \n{response.RequestMessage.Headers} \n");
            TrafficLogger.Message($"RESPONSE \n Status Code: \n{(int)response.StatusCode}({response.StatusCode}) \n Body: \n{responseString}\n");
            response.VerifyResponse(Description, uri);
            return responseString;
        }        

        /// <summary>
        /// Create a new entity
        /// </summary>
        /// <param name="overrideParameters">Dictionary of JSON key values to set/override of default JSON values</param>
        /// <returns>Id of Entity</returns>
        public string CreateId(string requestBody)
        {           
            byte[] messageBody = Encoding.UTF8.GetBytes(requestBody);
            ByteArrayContent content = new ByteArrayContent(messageBody);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = client.PostAsync(ApiPath(), content).Result;
            string responseString = response.Content.ReadAsStringAsync().Result;
            TrafficLogger.Message($"REQUEST POST {ApiPath()} \n Headers: \n{content.Headers} \n Body: \n{requestBody}\n");
            TrafficLogger.Message($"RESPONSE \n Status Code: \n{(int)response.StatusCode}({response.StatusCode}) \n Body: \n{responseString}\n");
            response.VerifyResponse(Description, ApiPath(), requestBody);
            content.Dispose();
            return responseString;
        }
    }
}
