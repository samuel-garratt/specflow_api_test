﻿using SpecFlowApiTest.Lib.ApiObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace SpecFlowApiTests.Lib.ApiObjects
{
    class PetStoreApiService : BaseApiService
    {
        public override string SubPath => "pet";

        public override string Description => "Pet";

        /// <summary>
        /// Create a new entity
        /// </summary>
        /// <param name="Name">Name of puppy</param>        
        /// <returns>Id of Entity</returns>
        public string Create(PetStoreDto petStore)
        {            
            return CreateId(petStore.CreateObj());
        }       
    }    
}
