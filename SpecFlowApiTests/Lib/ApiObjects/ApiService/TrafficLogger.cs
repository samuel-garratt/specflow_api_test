using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SpecFlowApiTests.Lib.ApiObjects
{
    // Used for recording API traffic in a log file
    class TrafficLogger
    {
        public readonly static char separator = Path.DirectorySeparatorChar;
        private readonly static string startupPath = Directory.GetCurrentDirectory();
        private readonly static string startupPathName = Directory.GetParent(startupPath).Parent.FullName;
        public readonly static string projectDirectory = Directory.GetParent(startupPathName).FullName;
        private readonly static string dataFolder = $"{projectDirectory}{separator}Logs{separator}";

        public static string LogFileName { get; set;}

        public static void Message(string message)
        {
            string filePath = Path.Combine(dataFolder, LogFileName);            
            using (StreamWriter outputFile = new StreamWriter(filePath, true))
            {
                outputFile.WriteLine($"{DateTime.Now} {message}");
            }
        }
    }
}
