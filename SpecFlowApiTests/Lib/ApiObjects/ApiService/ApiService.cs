﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpecFlowApiTests.Lib.ApiObjects
{
    /// <summary>
    /// Convience serivce class to handle talking with API objects globally
    /// </summary>
    class ApiService
    {
        /// <summary>
        /// Create Pet from a model of the pet properties
        /// </summary>
        /// <param name="petStore">Object for defining building a pet</param>
        /// <returns>Response from creation of Pet</returns>
        public static string Create(PetStoreDto petStore)
        {
            return new PetStoreApiService().Create(petStore);
        }

        /// <summary>
        /// Get a Pet that has the provided Id
        /// </summary>
        /// <param name="petId"></param>
        /// <returns></returns>
        public static string GetPet(string petId)
        {
            return new PetStoreApiService().Get(petId);
        }

        public const string ApiHost = "https://petstore.swagger.io/v2/";
    }
}
