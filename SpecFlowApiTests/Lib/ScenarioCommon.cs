using System;
using System.Collections.Generic;
using System.Text;

namespace SpecFlowApiTest.Lib
{
  /// <summary>
  /// Holds information to be shared across a SpecFlow scenario.
  /// This does the same as ScenarioContext but shows usage clearer rather than using a list
  /// </summary>
  class ScenarioCommon
  {
    /// <summary>
    /// Used to store response body retrieved in one step to be used in a further step definition
    /// </summary>
    public static string ResponseBody { get; set; }

    /// <summary>
    /// Used to store id retrieved in one step to be used in a further step definition
    /// </summary>
    public static string ResponseId { get; set; }    
  }
}
