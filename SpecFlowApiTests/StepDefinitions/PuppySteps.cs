﻿using SpecFlowApiTests.Lib.ApiObjects; // Added to reference the Api Object we defined previously
using TechTalk.SpecFlow;
using System.Collections.Generic; // Used for creating dictionaries
using Newtonsoft.Json.Linq; // Used for creating JSON tokens
using Newtonsoft.Json;      // Used for deserialising JSON to C# objects
using FluentAssertions;     // Used for making assertions

namespace SpecFlowApiTests.StepDefinitions
{
    [Binding]
    public class PuppySteps
    {
        // Following variables are shared across steps within a scenario
        private string _name = "Default name"; // Name of the puppy
        private PetStoreDto _storeResult;         // PetStore object with Id, Name, Status properties

        [Given(@"I want a puppy with a name of '(.*)'")]
        public void GivenIWantAPuppyWithANameOf(string name)
        {
            // Store name so it can be used in a future step
            _name = name;
        }

        // Example of 2 steps having same code
        [Given(@"I order a Puppy")]
        [When(@"I order a Puppy")]
        public void WhenIOrderAPuppy()
        {
            // Send a POST to PetStore API, storing response body in variable 'responseBody'
            var petstore = new PetStoreDto
            {
                Name = _name
            };
            var responseBody = ApiService.Create(petstore); 
            // Convert response body from String to a 'PetStore' object
            _storeResult = JsonConvert.DeserializeObject<PetStoreDto>(responseBody);
        }

        [When(@"I pick them up")]
        public void WhenIPickThemUp()
        {
            // Send a GET to PetStore API, using unique id from response
            var responseBody = ApiService.GetPet(_storeResult.Id);
            // Convert response body from String to a 'PetStore' object
            _storeResult = JsonConvert.DeserializeObject<PetStoreDto>(responseBody);
        }

        [Then(@"the puppy has the name specified")]
        public void ThenThePuppyHasTheNameSpecified()
        {
            // Assert that the name of the PetStore result is the same as the one set
            _storeResult.Name.Should().Be(_name);
        }
       
        [Then(@"their status should be available")]
        public void ThenTheirStatusShouldBeAvailable()
        {
            // Assert that the name of the PetStore result is the same as the one set
            _storeResult.Status.Should().Be("available");
        }
    }
}

