using SpecFlowApiTest.Lib;
using Newtonsoft.Json;
using SpecFlowApiTests.Lib.ApiObjects;
using System;
using System.Net.Http;
using TechTalk.SpecFlow;

namespace Hooks
{
  [Binding]
  public class SetUp
  {    
    public SetUp()
    {  
      JsonConvert.DefaultSettings = () => new JsonSerializerSettings
      {
        MissingMemberHandling = MissingMemberHandling.Ignore
      };            
    }
       
    [BeforeTestRun]
    public static void BeforeTestRun()
    {
        TrafficLogger.LogFileName = $"ApiTraffic_{DateTime.Now.ToString("yy_mm_dd_HH_mm_ss")}.log";
        GetInformation.Client = new HttpClient();
    }

    [BeforeScenario]
    public void Before()
    {
      // Reset variables stored across scenario
      ScenarioCommon.ResponseId = null;
      ScenarioCommon.ResponseBody = null;      
    }    
  }
}
